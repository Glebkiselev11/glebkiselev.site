//Подключаем модули галпа
const gulp = require('gulp');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const del = require('del');
const browserSync = require('browser-sync').create();
const imagemin = require('gulp-imagemin');
const rename = require("gulp-rename");
const fileinclude = require('gulp-file-include');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');




// Порядок подключения стайлов
const cssFiles = [
    './src/css/style.css'
    //'./src/css/media.css'
]

// Порядок подключения js файлов
const jsFiles = [
    './src/js/*.js'
    //'./src/js/main.js'
]


//конвертация сасс
gulp.task('sass-compile', function() {
    return gulp.src('./src/sass/**/*.sass')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./src/css/'))
})



//Таск на стили CSS
function styles() {
    //Шаблон для поиска файлов CSS
    //Все файлы по щаблону './src/css/**/*.css'
    return gulp.src(cssFiles)
    //Объединение файлов в один
    //.pipe(concat('style.css'))
    //Добавление префиксов
    .pipe(autoprefixer({
        browsers: ['last 2 version'],
        cascade: false
    }))
    //Минификация CSS
    /*
    .pipe(cleanCSS({
        level: 2
    }))
    */
    //Выходная папка для стилей
    .pipe(gulp.dest('./build/css'))
    .pipe(browserSync.stream());
}

//Таск на скрипты JS
function scripts() {
    //Шаблон для поиска файлов JS
    //Все файлы по шаблону './src/js/**/*.js'
    return gulp.src(jsFiles)
    //Объединение файлов в один
    .pipe(concat('script.js'))
    //Минификация JS
    /*
    .pipe(uglify({
        toplevel: true
    }))
    */
    //Выходная папка для стилей
    .pipe(gulp.dest('./build/js'))
    .pipe(browserSync.stream());
}

//Таск на изображения
function img() {
    return gulp.src('src/img/**/*')
        .pipe(imagemin([
            imagemin.jpegtran({progressive:true})
        ]))
        .pipe(gulp.dest('build/img'))
        .pipe(browserSync.stream());
}

// таск на HTML
function htmlbuild () {
    //Выберем файлы по нужному пути
    return gulp.src([
        'src/index.html',
        'src/pages/[^_]*.html'
    ]) 
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest('build')) //Выплюнем их в папку build
        .pipe(browserSync.stream());
}

// таск на шрифты
function fontbuild () {
    return gulp.src('src/fonts/*.ttf') //Выберем файлы по нужному пути
        .pipe(gulp.dest('build/fonts')) //Выплюнем их в папку build
        .pipe(browserSync.stream());
}



// Удалить в указанной папке
function clean(){
    return del(['build/*'])
}

//Просматривать файлы
function watch() {
    browserSync.init({
        server: {
            baseDir: "./build"
        }
    });
    //Следить за Sass файлами
    gulp.watch('./src/sass/**/*.sass', gulp.series('sass-compile'))
    //Следить за CSS файлами
    gulp.watch('./src/css/**/*.css', styles)
    //Следить за JS файлами
    gulp.watch('./src/js/**/*.js', scripts)
    //При изменении HTML запустить синхронизацию
    gulp.watch("./**/*.html").on('change', browserSync.reload);
    gulp.watch('./src/**/*.html', htmlbuild);
    //Следить за Картинками
    gulp.watch('./src/img', img);
}



//Таск Вызывающий функцию styles
gulp.task('styles', styles);
//Таск Вызывающий функцию scripts
gulp.task('scripts', scripts);
//Таск на перенос изображений
gulp.task('img', img);
//Таск для переноса Html файлов в папку build
gulp.task('htmlbuild', htmlbuild);
//Таск для переноса шрифтов в папку билд
gulp.task('fontbuild',fontbuild);
//Таск для очистки папки build
gulp.task('clean', clean);
//Таск для отселиживания изменений
gulp.task('watch', watch);
//Таск для удаления файлов в папке build и запуск Styles и Script
gulp.task('build', gulp.series(clean, gulp.parallel(styles,scripts,htmlbuild,fontbuild)));
//Таск запускает таск build и watch последовательно
gulp.task('dev', gulp.series('build', 'watch'))